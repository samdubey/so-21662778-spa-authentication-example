'use strict';

angular.module('SPAAuthenticationExampleApp')
  .controller('HeaderCtrl', ['$scope', 'User', function ($scope, User) {
    $scope.user = User.getUserData();
  }]);
